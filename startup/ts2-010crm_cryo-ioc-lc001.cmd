# @field SAVEFILE_DIR
# @type  STRING
# The directory where autosave should save files

# @field IOC
# @runtime YES

#- Load iocStats records
dbLoadTemplate(iocAdminSoft.substitutions, "IOC=$(IOC=TS2-010CRM:Cryo-IOC-LC001)")

requireSnippet(recSyncSetup.cmd)

#-# Configure autosave
#-# Number of sequenced backup files to write
#save_restoreSet_NumSeqFiles(1)
#
#-# Specify directories in which to search for request files
#set_requestfile_path("$(SAVEFILE_DIR)", "reqs")
#
#-# Specify where the save files should be
#set_savefile_path("$(SAVEFILE_DIR)", "")
#
#-# Specify what save files should be restored
#set_pass0_restoreFile("ts2-010crm_cryo-ioc-lc001.sav")
#
#-# Create monitor set
#system("mkdir -p '$(SAVEFILE_DIR)'/reqs")
#doAfterIocInit("makeAutosaveFileFromDbInfo('$(SAVEFILE_DIR)/reqs/ts2-010crm_cryo-ioc-lc001.req', 'autosaveFields_pass0')")
#doAfterIocInit("create_monitor_set('ts2-010crm_cryo-ioc-lc001.req', 1, '')")
